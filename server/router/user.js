const express = require('express')
const Result = require('../models/result')
const md5 = require('blueimp-md5')
const { body, validationResult } = require('express-validator')
const  boom  =  require('boom')
const { querySqlAll, querySqlOne, insertSql, insertImportMore, updateData, updateDataOne, updateWalletName, isSameName } = require('../db/index')
const { NETTYPE } = require('../utils/config');
const { Wallet, WiccApi, WaykiTransaction } = require("wicc-wallet-lib")

const router = express.Router()

var log = require('log4js').getLogger("user");

//测试
router.get('/info', function(req, res, next) {
    new  Result({  data: 'get请求成功' }, 'success').success(res)    

})


router.get('/addresslist', function(req, res, next) {
    querySqlAll('select id,mnemonic_id,address,mnemonic_offset,mnemonic_id,status,import_type,created_at,address_name,wallet_name from address where status = 200').then(reswallet => { 
        new  Result({  data: reswallet }, 'success').success(res)           
    }).catch(err  =>  {        
        //console.log(err)  
        log.error(err);  
    })    

})


router.get('/coinlist', function(req, res, next) {
    querySqlAll('select * from coin').then(rescoin => { 
        new  Result({  data: rescoin }, 'success').success(res)           
    }).catch(err  =>  {        
        // console.log(err) 
        log.error(err);   
    })    

})


router.post(
    '/transfertx', [
        body('nValidHeight').isNumeric().withMessage('区块高度类型不正确'),
        body('fees').isNumeric().withMessage('矿工费类型不正确')
    ],
    function(req, res, next) {
        const err = validationResult(req)
        if (!err.isEmpty()) {
            const [{ msg }] = err.errors //通过解构的方式拿到msg
                //错误信息借助boom实现
            next(boom.badRequest(msg)) //badRequest 参数异常
        } else {
            let { nTxType, nValidHeight, fees, dests, memo, feeSymbol, addressValue } = req.body

            querySqlOne('select * from address where address=?', addressValue).then(raddressitem => { 
                //网络初始化
                var arg = { network: NETTYPE }
                var wiccApi = new WiccApi(arg)

                if (raddressitem.priv_key) { //有私钥时
                    var privKeyWIF = raddressitem.priv_key
                    var publickey = raddressitem.pub_key
                    let wallet = new Wallet(privKeyWIF)

                    let cointransferTxinfo = {
                        nTxType: nTxType,
                        nValidHeight: nValidHeight,
                        fees: fees,
                        //srcRegId: publickey, //When srcRegId is available, use RegID as priority since it cost 0.001 WICC trx fees otherwise it'll be 0.002 WICC
                        dests: dests,
                        memo: memo, // remark, optional field
                        feeSymbol: feeSymbol
                    };

                    let transaction = new WaykiTransaction(cointransferTxinfo, wallet)
                    let cointransferTx = transaction.genRawTx()
                    new  Result({  data: cointransferTx }, 'success').success(res) 
                } else {
                    querySqlOne('select * from wallet where id=?', raddressitem.mnemonic_id).then(resmnemonic => { 

                        let strMne = resmnemonic.mnemonic
                        var walletInfo = wiccApi.createWallet(strMne, '12345678')
                        var privKeyWIF = wiccApi.getHDPriKeyFromSeed(walletInfo.seedinfo, '12345678', raddressitem.mnemonic_offset)
                        var publickey = raddressitem.pub_key
                        let wallet = new Wallet(privKeyWIF)

                        let cointransferTxinfo = {
                            nTxType: nTxType,
                            nValidHeight: nValidHeight,
                            fees: fees,
                            //srcRegId: publickey, //When srcRegId is available, use RegID as priority since it cost 0.001 WICC trx fees otherwise it'll be 0.002 WICC
                            dests: dests,
                            memo: memo, // remark, optional field
                            feeSymbol: feeSymbol
                        };

                        let transaction = new WaykiTransaction(cointransferTxinfo, wallet)
                        let cointransferTx = transaction.genRawTx()
                        new  Result({  data: cointransferTx }, 'success').success(res)    

                    }).catch(err  =>  {  
                        log.error(err);      
                        //console.log(err)    
                    })         
                }

            }).catch(err  =>  { 
                log.error(err);       
                //console.log(err)    
            })    

        }
    })

//查重
router.post(
    '/namecheck',
    function(req, res) {
        let { walletName } = req.body
            //console.log("addressArr=>", addressArr)
        isSameName(walletName).then(ins => {
            new  Result({  data: ins }, 'success').success(res) 

        }).catch(err  =>  {        
            log.error(err);
            res.json({ code: 9000, msg: '插入数据异常' })

        })  

    })

router.post(
    '/create',
    function(req, res) {
        let { addressArr } = req.body
            //console.log("addressArr=>", addressArr)
        insertSql(addressArr).then(insert => {
            // console.log('==>', insert)
            new  Result('success').success(res)

        }).catch(err  =>  {        
            log.error(err);
            res.json({ code: 9000, msg: '插入数据异常' })

        })  

    })


router.post(
    '/moreimportkeys',
    function(req, res) {
        let { importKeyObj } = req.body

        insertImportMore(importKeyObj).then(insert => {
            if (insert.code === 100) {
                new  Result('该账户已存在，无需重复导入').existed(res)
            } else {
                new  Result('success').success(res)
            }
        }).catch(err  =>  {        
            log.error(err);
            res.json({ code: 9000, msg: '插入数据异常' })

        })  

    })

router.post(
    '/votetx',
    function(req, res) {

        let { nTxType, nValidHeight, fees, voteLists, memo, addressValue } = req.body

        querySqlOne('select * from address where address=?', addressValue).then(raddressitem => { 
            //网络初始化
            var arg = { network: NETTYPE }
            var wiccApi = new WiccApi(arg)

            if (raddressitem.priv_key) { //有私钥时
                var privKeyWIF = raddressitem.priv_key
                var publickey = raddressitem.pub_key
                let wallet = new Wallet(privKeyWIF)

                let cointransferTxinfo = {
                    nTxType: nTxType,
                    nValidHeight: nValidHeight,
                    fees: fees,
                    voteLists: voteLists
                };

                let transaction = new WaykiTransaction(cointransferTxinfo, wallet)
                let cointransferTx = transaction.genRawTx()
                new  Result({  data: cointransferTx }, 'success').success(res) 
            } else {
                querySqlOne('select * from wallet where id=?', raddressitem.mnemonic_id).then(resmnemonic => { 

                    let strMne = resmnemonic.mnemonic
                    var walletInfo = wiccApi.createWallet(strMne, '12345678')
                    var privKeyWIF = wiccApi.getHDPriKeyFromSeed(walletInfo.seedinfo, '12345678', raddressitem.mnemonic_offset)
                    var publickey = raddressitem.pub_key
                    let wallet = new Wallet(privKeyWIF)

                    let cointransferTxinfo = {
                        nTxType: nTxType,
                        nValidHeight: nValidHeight,
                        fees: fees,
                        voteLists: voteLists
                    };

                    let transaction = new WaykiTransaction(cointransferTxinfo, wallet)
                    let cointransferTx = transaction.genRawTx()
                    new  Result({  data: cointransferTx }, 'success').success(res)    

                }).catch(err  =>  {   
                    log.error(err);     
                    //console.log(err)    
                })         
            }

        }).catch(err  =>  {     
            log.error(err);   
            //console.log(err)    
        })    


    })


router.post(
    '/cdptx',
    function(req, res) {

        let { nTxType, nValidHeight, cdpTxId, fees, feeSymbol, memo, addressValue, assetAmount, sCoinToMint } = req.body

        querySqlOne('select * from address where address=?', addressValue).then(raddressitem => { 
            //网络初始化
            var arg = { network: NETTYPE }
            var wiccApi = new WiccApi(arg)

            if (raddressitem.priv_key) { //有私钥时
                var privKeyWIF = raddressitem.priv_key
                var publickey = raddressitem.pub_key
                let wallet = new Wallet(privKeyWIF)

                var assetSymbol = "WICC"
                var map = new Map([
                    [assetSymbol, assetAmount]
                ])
                let cdpStakeTxinfo = {
                    nTxType: nTxType,
                    nValidHeight: nValidHeight,
                    fees: fees,
                    feeSymbol: feeSymbol,
                    cdpTxId: cdpTxId,
                    assetMap: map,
                    sCoinSymbol: "WUSD",
                    sCoinToMint: sCoinToMint
                };

                let transaction = new WaykiTransaction(cdpStakeTxinfo, wallet)
                let cointransferTx = transaction.genRawTx()
                new  Result({  data: cointransferTx }, 'success').success(res) 
            } else {
                querySqlOne('select * from wallet where id=?', raddressitem.mnemonic_id).then(resmnemonic => { 

                    let strMne = resmnemonic.mnemonic
                    var walletInfo = wiccApi.createWallet(strMne, '12345678')
                    var privKeyWIF = wiccApi.getHDPriKeyFromSeed(walletInfo.seedinfo, '12345678', raddressitem.mnemonic_offset)
                    var publickey = raddressitem.pub_key
                    let wallet = new Wallet(privKeyWIF)

                    var assetSymbol = "WICC"
                    var map = new Map([
                        [assetSymbol, assetAmount]
                    ])
                    let cdpStakeTxinfo = {
                        nTxType: nTxType,
                        nValidHeight: nValidHeight,
                        fees: fees,
                        feeSymbol: feeSymbol,
                        cdpTxId: cdpTxId,
                        assetMap: map,
                        sCoinSymbol: "WUSD",
                        sCoinToMint: sCoinToMint
                    };
                    //console.log(cdpStakeTxinfo)
                    let transaction = new WaykiTransaction(cdpStakeTxinfo, wallet)
                    let cointransferTx = transaction.genRawTx()
                    new  Result({  data: cointransferTx }, 'success').success(res)     

                }).catch(err  =>  {  
                    log.error(err);      
                    //console.log(err)    
                })         
            }

        }).catch(err  =>  {    
            log.error(err);    
            //console.log(err)    
        })    


    })


router.post(
    '/cdpredeemtx',
    function(req, res) {

        let { nTxType, nValidHeight, cdpTxId, fees, feeSymbol, memo, addressValue, assetAmount, sCoinsToRepay } = req.body

        querySqlOne('select * from address where address=?', addressValue).then(raddressitem => { 
            //网络初始化
            var arg = { network: NETTYPE }
            var wiccApi = new WiccApi(arg)

            if (raddressitem.priv_key) { //有私钥时
                var privKeyWIF = raddressitem.priv_key
                var publickey = raddressitem.pub_key
                let wallet = new Wallet(privKeyWIF)

                var assetSymbol = "WICC"
                var map = new Map([
                    [assetSymbol, assetAmount]
                ])
                let cdpRedeemTxinfo = {
                    nTxType: nTxType,
                    nValidHeight: nValidHeight,
                    fees: fees,
                    cdpTxId: cdpTxId,
                    feeSymbol: feeSymbol,
                    assetMap: map,
                    sCoinsToRepay: sCoinsToRepay,
                };

                let transaction = new WaykiTransaction(cdpRedeemTxinfo, wallet)
                let cointransferTx = transaction.genRawTx()
                new  Result({  data: cointransferTx }, 'success').success(res) 
            } else {
                querySqlOne('select * from wallet where id=?', raddressitem.mnemonic_id).then(resmnemonic => { 

                    let strMne = resmnemonic.mnemonic
                    var walletInfo = wiccApi.createWallet(strMne, '12345678')
                    var privKeyWIF = wiccApi.getHDPriKeyFromSeed(walletInfo.seedinfo, '12345678', raddressitem.mnemonic_offset)
                    var publickey = raddressitem.pub_key
                    let wallet = new Wallet(privKeyWIF)

                    var assetSymbol = "WICC"
                    var map = new Map([
                        [assetSymbol, assetAmount]
                    ])
                    let cdpRedeemTxinfo = {
                        nTxType: nTxType,
                        nValidHeight: nValidHeight,
                        fees: fees,
                        cdpTxId: cdpTxId,
                        feeSymbol: feeSymbol,
                        assetMap: map,
                        sCoinsToRepay: sCoinsToRepay,
                    };

                    let transaction = new WaykiTransaction(cdpRedeemTxinfo, wallet)
                    let cointransferTx = transaction.genRawTx()
                    new  Result({  data: cointransferTx }, 'success').success(res)     

                }).catch(err  =>  {  
                    log.error(err);      
                    //console.log(err)    
                })         
            }

        }).catch(err  =>  {     
            log.error(err);   
            //console.log(err)    
        })    


    })

router.post(
    '/login',
    function(req, res) {
        let { password } = req.body
            //console.log("addressArr=>", addressArr)
        let has = md5(password) //默认32位
        querySqlAll('select * from account').then(count => { 
            if (count[0]['lock_key'] == has) {
                new  Result({  data: '验证成功' }, 'success').success(res) 
            } else {
                res.json({ code: 404, msg: '密码错误' })
            }          
        }).catch(err  =>  {        
            // console.log(err) 
            log.error(err);   
        })    


    })
router.post(
    '/resetPwd',
    function(req, res) {
        let { password, resetpassword } = req.body
            //console.log("addressArr=>", addressArr)
        let has = md5(password) //默认32位
        let newpwd = md5(resetpassword)
        querySqlAll('select * from account').then(count => { 
            if (count[0]['lock_key'] == has) {

                updateData(`update account set lock_key=? where lock_key=?`, newpwd, has).then(getda => {
                    if (getda.code == 0) {
                        new  Result({  data: '密码设置成功' }, 'success').success(res) 
                    } else {
                        res.json({ code: -1, msg: '密码设置失败' })
                    }
                })
            } else {
                res.json({ code: 404, msg: '原密码输入错误' })
            }          
        }).catch(err  =>  {        
            // console.log(err) 
            log.error(err);   
        })    


    })
router.post(
    '/showWalletKeys',
    function(req, res) {
        let { password, value, importType, mnemonic_id } = req.body
        let has = md5(password) //默认32位
        querySqlAll('select * from account').then(count => { 
            if (count[0]['lock_key'] == has) {
                if (importType != 'private') {
                    querySqlOne('select * from wallet where id=?', mnemonic_id).then(additem => { 

                        new  Result({  data: additem.mnemonic }, 'success').success(res)     

                    }).catch(err  =>  {  
                        log.error(err);      
                        //console.log(err)    
                    })     
                } else {
                    querySqlOne('select * from address where address=?', value).then(additem => { 

                        new  Result({  data: additem.priv_key }, 'success').success(res)     

                    }).catch(err  =>  {  
                        log.error(err);      
                        //console.log(err)    
                    })     

                }


            } else {
                res.json({ code: 404, msg: '密码错误' })
            }          
        }).catch(err  =>  {        
            // console.log(err) 
            log.error(err);   
        })    


    })

router.post(
    '/updateaddressname',
    function(req, res) {
        let { oldname, newname, address } = req.body

        updateDataOne(`update address set address_name=? where address_name=? and address=?`, newname, oldname, address).then(getda => {
            if (getda.code == 0) {
                new  Result({  data: '设置成功' }, 'success').success(res) 
            } else {
                res.json({ code: -1, msg: '密码设置失败' })
            }
        }).catch(err  =>  {        
            // console.log(err) 
            log.error(err);   
        })    
    })
router.post(
    '/updatewalletname',
    function(req, res) {
        let { oldname, newname } = req.body

        updateWalletName(newname, oldname).then(getda => {
            if (getda.code == 0) {
                new  Result({  data: '设置成功' }, 'success').success(res) 
            } else if (getda.code == 101) {
                res.json({ code: -1, msg: '昵称已存在' })
            } else {
                res.json({ code: -1, msg: '设置失败' })
            }
        }).catch(err  =>  {        
            // console.log(err) 
            log.error(err);   
        })    
    })


module.exports = router