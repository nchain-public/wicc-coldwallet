var log = require('log4js').getLogger("dbIndex");
const { FILEPATH } = require('../utils/config');
const sqlite3 = require('sqlite3').verbose();

//查询所有数据
function querySqlAll(sql) {
    let db = new sqlite3.Database(FILEPATH);
    return new Promise((resolve, reject) => {
        try {
            //"select * from user"
            db.all(sql, function(err, row) {
                //console.log("=>", JSON.stringify(row));
                if (err) {
                    log.error(err);
                    reject(err)
                } else {
                    resolve(row)
                }
            })
        } catch (e) {
            log.error(e);
            reject(e)
        } finally {
            db.close()
        }
    })
}

//根据条件查询
function querySqlOne(sql, result) {
    let db = new sqlite3.Database(FILEPATH);
    return new Promise((resolve, reject) => {
        try {
            //"select * from user where username=?"
            db.each(sql, result, function(err, row) {
                if (err) {
                    log.error(err);
                    reject(err)
                } else {
                    resolve(row)
                }
            })
        } catch (e) {
            log.error(e);
            reject(e)
        } finally {
            db.close()
        }
    })
}



//插入数据到数据库
function insertSql(result) {
    let db = new sqlite3.Database(FILEPATH);
    return new Promise((resolve, reject) => {
        try {
            let a = result.length
                //console.log("a=>", result)
            if (a == 0) {
                log.error({ code: 9000, msg: '传输给后台的参数有误' });
                reject({ code: 9000, msg: '传输给后台的参数有误' })
            }
            //插入助记词到mnemonic
            var add_mnemonic = db.prepare('insert into wallet (mnemonic,status) values (?,?)')
            add_mnemonic.run(result[0].mnemonic, 200, function(err, row) {

                if (err) {
                    log.error(err);
                    reject(err)
                } else {

                    querySqlOne('select * from wallet where mnemonic=?', result[0].mnemonic).then(resmnemonic => {
                        var walletname = ''
                        if (result[0].wallet_name != '钱包') {
                            walletname = result[0].wallet_name
                        } else {
                            walletname = '钱包' + resmnemonic.id
                        }
                        for (var i = 0; i < result.length; i++) {

                            var sql_add = db.prepare('insert into address (address,mnemonic_offset, mnemonic_id, pub_key,wallet_name,address_name,status) values (?,?,?,?,?,?,?)')

                            sql_add.run(result[i].address, result[i].indexaddress, resmnemonic.id, result[i].publickey, walletname, '匿名地址', 200, function(err, row) {
                                a--
                                if (!err && a <= 0) {
                                    resolve({ code: 0 })
                                }
                                if (err) {
                                    log.error(err);
                                    reject(err)
                                }

                            })

                        }

                         
                    }).catch(err => {
                        log.error(err);
                        reject(err)
                    })

                }
            })


        } catch (e) {
            log.error(e);
            reject(e)
        } finally {
            //db.close()
        }
    })
}

//修改数据
function updateData(sql, newpwd, oldpwd) {
    let db = new sqlite3.Database(FILEPATH);
    return new Promise((resolve, reject) => {
        try {

            var sql_modify = db.prepare(sql)
            sql_modify.run(newpwd, oldpwd, function(err, row) {
                if (!err) {
                    resolve({ code: 0 })
                } else {
                    log.error(err);
                    reject(err)
                }
            })
        } catch (e) {
            log.error(e);
            reject(e)
        } finally {

        }
    })
}
//修改钱包名称
function updateWalletName(newname, oldname) {
    let db = new sqlite3.Database(FILEPATH);
    return new Promise((resolve, reject) => {
        try {
            db.all("select wallet_name from address where wallet_name=?", newname, function(er, ro) {
                if (!er) {
                    if (ro.length == 0) {
                        var sql_modify = db.prepare('update address set wallet_name=? where wallet_name=?')
                        sql_modify.run(newname, oldname, function(err, row) {
                            if (!err) {
                                resolve({ code: 0 })
                            } else {
                                log.error(err);
                                reject(err)
                            }
                        })

                    } else {
                        resolve({ code: 101 })
                    }
                } else {
                    log.error(err);
                    reject(err)
                }

            })

        } catch (e) {
            log.error(e);
            reject(e)
        } finally {

        }
    })
}
//账号命名查重
function isSameName(name) {
    let db = new sqlite3.Database(FILEPATH);
    return new Promise((resolve, reject) => {
        try {
            db.all("select wallet_name from address where wallet_name=?", name, function(er, ro) {
                if (!er) {
                    if (ro.length == 0) {
                        resolve({ code: 0 })
                    } else {
                        resolve({ code: 101 })
                    }
                } else {
                    log.error(err);
                    reject(err)
                }

            })

        } catch (e) {
            log.error(e);
            reject(e)
        } finally {

        }
    })
}

function updateDataOne(sql, newpwd, oldpwd, address) {
    let db = new sqlite3.Database(FILEPATH);
    return new Promise((resolve, reject) => {
        try {

            var sql_modify = db.prepare(sql)
            sql_modify.run(newpwd, oldpwd, address, function(err, row) {
                if (!err) {
                    resolve({ code: 0 })
                } else {
                    log.error(err);
                    reject(err)
                }
            })
        } catch (e) {
            log.error(e);
            reject(e)
        } finally {

        }
    })
}

//批量导入助记词
function insertImportMore(resultObj) {
    let db = new sqlite3.Database(FILEPATH);
    return new Promise((resolve, reject) => {
        try {

            if (resultObj.mnemonic) { //导入的是助记词
                let b = resultObj.childArr.length
                let c = resultObj.childArr.length
                db.all("select * from wallet where mnemonic=?", resultObj.mnemonic, function(err, mnemo) {
                    if (!err) {

                        if (mnemo.length == 0) { //不是公用的助记词

                            var add_mnemonic = db.prepare('insert into wallet (mnemonic,status) values (?,?)')
                            add_mnemonic.run(resultObj.mnemonic, 200, function(err, adrow) {
                                if (err) {
                                    reject(err)
                                } else {
                                    querySqlOne('select * from wallet where mnemonic=?', resultObj.mnemonic).then(resmnemonic => {
                                        for (var i = 0; i < resultObj.childArr.length; i++) {

                                            var sql_add = db.prepare('insert into address (address,mnemonic_offset, mnemonic_id, pub_key,status,address_name,wallet_name) values (?,?,?,?,?,?,?)')
                                            sql_add.run(resultObj.childArr[i]['address'], resultObj.childArr[i]['indexaddress'], resmnemonic.id, resultObj.childArr[i]['publickey'], 200, resultObj.childArr[i]['address_name'], `${resultObj.childArr[i]['wallet_name']}${resmnemonic.id}`, function(err, row) {
                                                b--
                                                if (!err && b <= 0) {
                                                    resolve({ code: 0 })
                                                }
                                                if (err) {
                                                    log.error(err);
                                                    reject(err)
                                                }

                                            }) 
                                        }

                                    }).catch(err => {
                                        log.error(err);
                                        reject(err)
                                    })

                                }
                            })
                        } else { //有公用的助记词
                            querySqlOne('select * from wallet where mnemonic=?', resultObj.mnemonic).then(resmnemonic => {

                                db.all("select address from address where mnemonic_id=?", resmnemonic.id, function(err, row) {
                                    var newarr = row.map(item => {
                                        return item.address
                                    })
                                    if (!err) {
                                        for (var i = 0; i < resultObj.childArr.length; i++) {

                                            if (newarr.indexOf(resultObj.childArr[i]['address']) < 0) { //是否有该地址

                                                var sql_add = db.prepare('insert into address (address,mnemonic_offset, mnemonic_id, pub_key,status,address_name,wallet_name) values (?,?,?,?,?,?,?)')
                                                sql_add.run(resultObj.childArr[i]['address'], resultObj.childArr[i]['indexaddress'], resmnemonic.id, resultObj.childArr[i]['publickey'], 200, resultObj.childArr[i]['address_name'], `${resultObj.childArr[i]['wallet_name']}${resmnemonic.id}`, function(err, rw) {
                                                    --b
                                                    var al = b + c
                                                    if (!err && al == resultObj.childArr.length) {
                                                        resolve({ code: 0 })
                                                    }
                                                    if (err) {
                                                        log.error(err);
                                                        reject(err)
                                                    }

                                                }) 
                                            } else { //有地址
                                                --c
                                                var an = b + c
                                                if (c <= 0) {
                                                    resolve({ code: 100 })
                                                } else if (an == resultObj.childArr.length) {
                                                    resolve({ code: 0 })
                                                }
                                            }
                                        }
                                    } else {
                                        log.error(err);  
                                    }

                                });


                            }).catch(err => {
                                log.error(err);
                                reject(err)
                            })
                        }

                    } else {
                        log.error(err);
                    }
                })



            } else if (resultObj.privatekey) { //导入的是私钥

                db.all("select address from address where address=?", resultObj.address, function(er, ro) {
                    var newarr = ro.map(item => {
                        return item.address
                    })

                    if (!er && newarr.indexOf(resultObj.address) < 0) {
                        var private_add = db.prepare('insert into address (address,priv_key,pub_key,import_type,status,address_name,wallet_name) values (?,?,?,?,?,?,?)')
                        private_add.run(resultObj.address, resultObj.privatekey, resultObj.publickey, 'private', 200, resultObj.address_name, resultObj.wallet_name, function(err, row) {

                            if (!err) {
                                resolve({ code: 0 })
                            } else {
                                log.error(err);
                                reject(err)
                            }

                        }) 
                    } else if (newarr.indexOf(resultObj.address) > -1) {

                        resolve({ code: 100 }) //已存在
                    } else {

                        log.error(err);
                        reject(err)
                    }
                })


            } else {
                log.error({ code: 9001, msg: '导入的参数有误' });
                reject({ code: 9001, msg: '导入的参数有误' })
            }

        } catch (e) {
            log.error(e);
            reject(e)
        } finally {

        }
    })
}

module.exports = {
    querySqlAll,
    querySqlOne,
    insertSql,
    insertImportMore,
    updateData,
    updateDataOne,
    updateWalletName,
    isSameName
}