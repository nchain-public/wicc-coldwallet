cp -r ../server .

sed -i '' -e "s/FILEPATH: \"[^\"]*\"/FILEPATH: \"\/sqlite\/coolwalletprod.db\"/" \
	-e "s/testnet/mainnet/" \
	./server/utils/config.js
	
docker build -t wicc/mcw-nodejs .
rm -rf ./server