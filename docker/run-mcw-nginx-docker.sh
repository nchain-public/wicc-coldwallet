WEB_DIR=~/MyColdWallet/wicc-coldwallet/webui

cd ~/MyColdWallet/wicc-coldwallet/docker

sed -i '' -e "s/baas-test/baas/" -e "s/testnet/mainnet/" \
	../webui/src/tools/config.js

docker run -it --rm -v $WEB_DIR:/web \
   wicc/mcw-nodejs sh -c "cd /web && cnpm install && cnpm run build"

docker stop wicc-mcw-webui && docker rm wicc-mcw-webui

docker run -d --restart always \
	--name "wicc-mcw-webui" \
	-p 8080:80 \
	-v $WEB_DIR/dist:/var/www/public \
	-v $(pwd)/nginx.conf:/etc/nginx/conf.d/default.conf \
	--link wicc-mcw-nodejs:nodejs \
	nginx;