import Vue from 'vue'
import Router from 'vue-router'

const originalPush = Router.prototype.push
Router.prototype.push = function push(location, onResolve, onReject) {
    if (onResolve || onReject) return originalPush.call(this, location, onResolve, onReject)
    return originalPush.call(this, location).catch(err => err)
}

Vue.use(Router)

export default new Router({
    routes: [{
            path: '/',
            redirect: '/exchange',
            component: resolve => require(['@/pages/index.vue'], resolve),
            children: [{
                    path: 'exchange',
                    name: 'exchange',
                    component: resolve => require(['@/pages/exchange/index.vue'], resolve)
                },
                {
                    path: 'createwallet',
                    name: 'createwallet',
                    //component: resolve => require(['@/pages/wallet/createwallet.vue'], resolve)
                    component: resolve => require(['@/pages/wallet/index.vue'], resolve)
                }
            ]
        },
        {
            path: '/lock',
            component: resolve => require(['@/common/lock.vue'], resolve),
        }

    ]
})