import axios from "axios"
import { baseUrl } from "./config"
import { Message } from 'element-ui';
let getLocalTimeZone = () => {
    return (new Date().getTimezoneOffset() / 60) * -1
};

let uuid = () => {
    let mydate = new Date()
    return "kz" + mydate.getDay() + mydate.getHours() + mydate.getMinutes() + mydate.getSeconds() + mydate.getMilliseconds() + Math.round(Math.random() * 10000000000000);
};


axios.defaults.headers.common['user_timezone'] = `${getLocalTimeZone()}`;
axios.defaults.headers.common['platform_code'] = "web";
axios.defaults.headers.post['Content-Type'] = 'application/json';
axios.defaults.timeout = 30000;
axios.defaults.baseURL = baseUrl;
// 添加请求拦截器
axios.interceptors.request.use(function(config) {

    let device_uuid_local = localStorage.getItem('device_uuid')
    let device_uuid = ''
    if (!device_uuid_local) {
        device_uuid = 'web' + uuid()
        localStorage.setItem('device_uuid', device_uuid)
    } else {
        device_uuid = device_uuid_local
    }
    config.headers['device_uuid'] = device_uuid
    config.headers['request_uuid'] = uuid()
    config.headers['platform_code'] = 'web'
    return config;
}, function(error) {
    // 对请求错误做些什么
    return Promise.reject(error);
});

// 添加响应拦截器
axios.interceptors.response.use(function(response) {
    // 对响应数据做点什么

    return response;
}, function(error) {
    // 对响应错误做点什么
    return Promise.reject(error);
});

export default {
    get(url, data, config) {
        let httpConfig = {}
        if (config) {
            for (let item in config) {
                httpConfig[item] = config[item]
            }
        }
        if (data) {
            data.ts = new Date().getTime()
            httpConfig.params = data
        } else {
            let params = { ts: new Date().getTime() }
            httpConfig.params = params
        }
        return new Promise((resolve, reject) => {

            axios.get(url, httpConfig)
                .then(response => {
                    if (response.data.code == 2011) {
                        Message({
                            showClose: true,
                            message: '登录超时，请重新登录',
                            type: 'error'
                        });
                    } else {
                        resolve(response.data);
                    }

                })
                .catch((error) => {
                    let str = error.toString()
                    if (str.search('timeout') > -1) {
                        Message({
                            showClose: true,
                            message: '请求超时',
                            type: 'error'
                        });
                    } else {
                        Message({
                            showClose: true,
                            message: "请求出错，请重试",
                            type: 'error'
                        });
                    }
                    reject(error)
                })
        })
    },
    post(url, data, config) {

        return new Promise((resolve, reject) => {

            axios.post(url, data, config)
                .then(response => {
                    if (response.data.code == 2011) {
                        Message({
                            showClose: true,
                            message: "登录超时，请重新登录",
                            type: 'error'
                        });
                    } else {
                        resolve(response.data);
                    }
                })
                .catch((error) => {
                    let str = error.toString()
                    if (str.search('timeout') > -1) {
                        Message({
                            showClose: true,
                            message: "请求超时",
                            type: 'error'
                        });

                    } else {
                        Message({
                            showClose: true,
                            message: "请求出错，请重试",
                            type: 'error'
                        });
                    }
                    reject(error)
                })


        })
    }
}