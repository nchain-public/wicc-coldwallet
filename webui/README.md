# wicc-coldwallet-webui

A cold wallet through a locally deployed web UI to interact with a Linux WICC coind JSON RPC

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```
```javascript
//账户结构
// options:[
//                 {
//                     value:'1',
//                     label:'账户1',
//                     children:[
//                         {value:'WREXW3YGNaudHdC9SXGeGxzwSukVqfeSTN',label:"WREXW3YGNaudHdC9SXGeGxzwSukVqfeSTN",addressIndex:0},
//                         {value:'WREXW3YGNaudHdC9SXGeGxzwSukVqfeSTa',label:"WREXW3YGNaudHdC9SXGeGxzwSukVqfeSTa",addressIndex:1},
//                         {value:'WREXW3YGNaudHdC9SXGeGxzwSukVqfeSTb',label:"WREXW3YGNaudHdC9SXGeGxzwSukVqfeSTb",addressIndex:2}
//                     ]
//                 },
//                 {
//                     value:'2',
//                     label:'账户2',
//                     children:[
//                         {value:'WREXW3YGNaudHdC9SXGeGxzwSukVqfeSTN1',label:"WREXW3YGNaudHdC9SXGeGxzwSukVqfeSTN1",addressIndex:0},
//                         {value:'WREXW3YGNaudHdC9SXGeGxzwSukVqfeSTa1',label:"WREXW3YGNaudHdC9SXGeGxzwSukVqfeSTa1",addressIndex:1},
//                         {value:'WREXW3YGNaudHdC9SXGeGxzwSukVqfeSTb1',label:"WREXW3YGNaudHdC9SXGeGxzwSukVqfeSTb1",addressIndex:2},
//                         {value:'WREXW3YGNaudHdC9SXGeGxzwSukVqfeSTc1',label:"WREXW3YGNaudHdC9SXGeGxzwSukVqfeSTc1",addressIndex:3},
//                     ]
//                 },
//                 {
//                     value:'3',
//                     label:'账户3',
//                     children:[
//                         {value:'WREXW3YGNaudHdC9SXGeGxzwSukVqfeSTN3',label:"WREXW3YGNaudHdC9SXGeGxzwSukVqfeSTN3",addressIndex:0},
//                         {value:'WREXW3YGNaudHdC9SXGeGxzwSukVqfeSTa3',label:"WREXW3YGNaudHdC9SXGeGxzwSukVqfeSTa3",addressIndex:1},
//                         {value:'WREXW3YGNaudHdC9SXGeGxzwSukVqfeSTb3',label:"WREXW3YGNaudHdC9SXGeGxzwSukVqfeSTb3",addressIndex:2},
//                         {value:'WREXW3YGNaudHdC9SXGeGxzwSukVqfeSTc3',label:"WREXW3YGNaudHdC9SXGeGxzwSukVqfeSTc3",addressIndex:3},
//                         {value:'WREXW3YGNaudHdC9SXGeGxzwSukVqfeSTc3',label:"WREXW3YGNaudHdC9SXGeGxzwSukVqfeSTc3",addressIndex:3},
//                         {value:'WREXW3YGNaudHdC9SXGeGxzwSukVqfeSTc4',label:"WREXW3YGNaudHdC9SXGeGxzwSukVqfeSTc4",addressIndex:3},
//                         {value:'WREXW3YGNaudHdC9SXGeGxzwSukVqfeSTc5',label:"WREXW3YGNaudHdC9SXGeGxzwSukVqfeSTc5",addressIndex:3},
//                     ]
//                 }
//             ],
```